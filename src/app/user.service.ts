import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
UserData : any = {};
  constructor() { }

  getUserData()
  {
    return this.UserData;
  }

  setData(data) {
    this.UserData = data;
  }
}
