import { Component, AfterViewInit, AfterViewChecked } from '@angular/core';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal/modal.component';
import {UserService} from './user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

title : any = "to our webPage";
  constructor(private modalService: NgbModal, private user : UserService) {
    setTimeout(() => {
      this.open();
    }, 3000);
 

  }
  

  open() {
    // const modalRef = this.modalService.open(ModalComponent);
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.title = 'User Form';
  }}
