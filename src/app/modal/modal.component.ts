import { Component,Input, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {UserService} from '../user.service';
import { Router} from '@angular/router';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() title = `Information`;

  userForm = new FormGroup({
    name : new FormControl('',Validators.required),
    mobile : new FormControl('',Validators.compose([
      Validators.required,
      Validators.pattern('^[7-9][0-9]{9}$')
])),
    email : new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
]))
  });
  constructor(
    public activeModal: NgbActiveModal,private user : UserService,private route : Router
  ) {}

  ngOnInit() {
  }

  save() {
    console.log(this.userForm.value);
    this.user.setData(this.userForm.value);
    this.route.navigateByUrl('/user');
  }
}
